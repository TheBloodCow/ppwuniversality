$(document).ready(function() {
  $(".backgroundColor").click(function() {
    $(".greetBox").toggleClass("greetBox-Alt");
      $(".backgroundColor").toggleClass("backgroundColor-alt");
        $(".greeting").toggleClass("greeting-Alt");
  });
});

$.ajax({
  url: "https://www.googleapis.com/books/v1/volumes?q=quilting",
  success: function(result) {
    for (let i = 0; i < result.items.length; i++) {
      $('.bookData').append(
        "<tr>\n" +
        "<td> <img src=\"" + result.items[i].volumeInfo.imageLinks.thumbnail + "\"></td>\n" +
        "<td>" + result.items[i].volumeInfo.title + "</td>\n" +
        "<td>" + result.items[i].volumeInfo.publishedDate + "</td>\n" +
        "<td>" + result.items[i].volumeInfo.publisher + "</td>\n" +
        "<td>" + "<td style='padding: 15px;' class='align-middle' style='text-align:center;'>" + "<img id='bintang" + i + "' onclick='favorite(this.id)' width='28px' src='https://image.flaticon.com/icons/svg/149/149222.svg'>" + "</td>\n" +
        "</tr>"
      )
    }
  }
});


function lookFor(find) {
  var searchQuery = $("input").val();
  $.ajax({
    url: "https://www.googleapis.com/books/v1/volumes?q=" + searchQuery,
    datatype: 'json',
    success: function(find) {
      $('.bookData').empty();
      for (var i = 0; i < find.items.length; i++) {
        $('.bookData').append(
          '<tr>' +
          '<td scope="row"><img src="' + find.items[i].volumeInfo.imageLinks.smallThumbnail + '"></td>' +
          '<td scope="row">' + find.items[i].volumeInfo.title + '</td>' +
          '<td scope="row">' + find.items[i].volumeInfo.description + '</td>' +
          '<td scope="row">' + find.items[i].volumeInfo.publisher + '</td>' +
          "<td style='padding: 15px;' class='align-middle' style='text-align:center;'>" + "<img id='bintang" + i + "' onclick='favorite(this.id)' width='28px' src='https://image.flaticon.com/icons/svg/149/149222.svg'>" + "</td></tr>")
      }
    }
  })
};
var counter = 0;

function favorite(clicked_id) {
  var btn = document.getElementById(clicked_id);
  if (btn.classList.contains("favourite")) {
    btn.classList.remove("favourite");
    document.getElementById(clicked_id).src = 'https://image.flaticon.com/icons/svg/149/149222.svg';
    counter--;
    document.getElementById("counter").innerHTML = counter;
  } else {
    btn.classList.add('favourite');
    document.getElementById(clicked_id).src = 'https://image.flaticon.com/icons/svg/291/291205.svg';
    counter++;
    document.getElementById("counter").innerHTML = counter;
  }
};

function look() {
  var word = $('input[name="pencarian"]').val();
  lookFor(word);
};
