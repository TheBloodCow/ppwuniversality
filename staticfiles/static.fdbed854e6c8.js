  $(document).ready(function() {
    $(".backgroundColor").click(function() {
      $(".followsYou").toggleClass("followsYou-alt");
    });
  });
  var counter = 0;
  $(document).ready(function() {
    $(document).on("click", ".bookFavorite", function() {
      $(".bookFavorite").toggleClass("bookFavorite-alt");
    });
  });
  $.ajax({
    url: "https://www.googleapis.com/books/v1/volumes?q=quilting",
    success: function(result) {
      for (let i = 0; i < result.items.length; i++) {
        $('.bookData').append(
          "<tr>\n" +
          "<td> <img src=\"" + result.items[i].volumeInfo.imageLinks.thumbnail + "\"></td>\n" +
          "<td>" + result.items[i].volumeInfo.title + "</td>\n" +
          "<td>" + result.items[i].volumeInfo.publishedDate + "</td>\n" +
          "<td>" + result.items[i].volumeInfo.publisher + "</td>\n" +
          "<td>" + "<a class=\"waves-effect waves-light btn bookFavorite\" id= "+i+">Favorite this book!</a>"+ "</td>\n" +
          "</tr>"
        )
      }
    }
  })
    function lookFor(find) {
        var searchQuery = $("input").val();
        $.ajax({
            url: "https://www.googleapis.com/books/v1/volumes?q=" + searchQuery,
            datatype: 'json',
            success: function (find) {
                $('#bookTable').empty();
                for (var i = 0; i < find.items.length; i++) {
                    $('#bookTable').append(
                        '<tr>'
                        + '<td class=""><img src="' + find.items[i].volumeInfo.imageLinks.smallThumbnail + '"></td>'
                        + '<td class="">' + find.items[i].volumeInfo.title + '</td>'
                        + '<td class="">' + find.items[i].volumeInfo.description + '</td>'
                        + '<td class="">' + find.items[i].volumeInfo.publisher + '</td>'
                        + "<td style='padding: 15px;' class='align-middle' style='text-align:center;'>" + "<img id='bintang" + i + "' onclick='favorite(this.id)' width='28px' src='https://image.flaticon.com/icons/svg/149/149222.svg'>" + "</td></tr>")
                }
            }
        })
    }
    function look() {
        var word = $('input[name="pencarian"]').val();
        lookFor(word);
    }
