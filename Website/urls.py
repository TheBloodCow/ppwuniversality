from django.urls import path
from django.conf.urls import url
from .views import profile, addPost, deleteAll, signUp, saveAccount, forum, saveproject, bookPage, login, information, logout

urlpatterns = [
    path('', profile, name ='profile'),
    path('addPost', addPost, name = 'addPost'),
    path('login', login, name = 'login'),
    path('logout', logout, name = 'logout'),
    path('deleteAll', deleteAll, name = 'deleteAll'),
    path('bookPage', bookPage, name = 'bookPage'),
    path('signUp', signUp, name='signUp'),
    path('saveAccount', saveAccount, name = 'saveAccount'),
    path('forum', forum, name='forum'),
    path('saveproject', saveproject, name='saveproject'),
    path('information', information, name='information'),
]
