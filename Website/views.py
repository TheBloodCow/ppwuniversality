from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse
from django.contrib.auth.models import User
from .models import OnePost, Account
from .forms import Add_Post, Add_Account_Form, LoginForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login as django_login
from django.contrib.auth import logout as django_logout
# Create your views here.

# Remember: EVERYTHING IN VIEWS MUST BE SPECIFIED IN URLS.PY OF THE PROJECT
response={}#<-----THIS IS ABSOLUTELY NECESSARY
def profile(request):
	return render(request, "homePage.html")
def forum(request):
	forum = OnePost.objects.all()
	response["forum"]=forum
	return render(request, "forum.html", response)
def information(request):
	return render(request, "DDDemo.html")
def login(request):
  if request.method == 'GET':
    form = LoginForm()
    return render(request, 'login.html', {'form': form})
  else:
    form = LoginForm(request.POST)
    if form.is_valid():
      cleaned_data = form.cleaned_data
      user = authenticate(username=cleaned_data['name'], password=cleaned_data['password'])
      if user is not None:
        django_login(request, user)
        return HttpResponseRedirect(reverse('profile'))
      else:
        return HttpResponseRedirect('login')
    return HttpResponseRedirect('login')
def logout(request):
	request.session.flush()
	return HttpResponseRedirect(reverse('profile'))
def addPost(request):
	response["addPost_form"] = Add_Post()
	return render(request, "addPost.html", response)
def signUp(request):
	response["subscribe_form"] = Add_Account_Form()
	return render(request, "signUp.html", response)
def deleteAll(request):
	forum = OnePost.objects.all().delete()
	return HttpResponseRedirect(reverse("forum"))
def bookPage(request):
	return render(request, "bookKeeping.html", response)
#--------------------------------------
def saveproject(request):
	form = Add_Post(request.POST or None)
	if(request.method == "POST" and form.is_valid()):
		response["Status"] = request.POST["Status"]
		project = OnePost(Status=response["Status"])
		project.save()
		return HttpResponseRedirect(reverse("forum"))
	else:
		return HttpResponseRedirect("/")
def saveAccount(request):
    response={}
    form = Add_Account_Form(request.POST or None)
    if(request.method == "POST" and form.is_valid()):
        username=request.POST["username"]
        first_name = request.POST["first_name"]
        last_name= request.POST["last_name"]
        email= request.POST["email"]
        password= request.POST["password"]
        password1= request.POST["password1"]
        birth_date = request.POST["birth_date"]
        user = User.objects.create(username = username, password = password, email = email, first_name=first_name, last_name=last_name,)
        user.set_password(password)
        user.save()
        account = Account(user = user,birth_date = birth_date)
        account.save()
        return HttpResponseRedirect("/")
    else:
        return HttpResponseRedirect("/")
