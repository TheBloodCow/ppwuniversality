from django import forms
from .models import OnePost
import datetime
from django import forms
from django.contrib.auth.models import User
from .models import Account

class Add_Post(forms.Form):
    error_messages = {
        'required': 'Please type',
    }
    pos_attrs = {
        'type': 'text',
        'class': 'todo-form-input',
        'placeholder':'Your status'
    }
    title_attrs = {
        'type': 'text',
        'class': 'todo-form-input',
        'placeholder':'Your Post'
    }

    Status = forms.CharField(label='', required=True, max_length=40, widget=forms.Textarea(attrs=pos_attrs))

class Add_Account_Form(forms.Form):
    Date = {
        'type': 'Date',
        'required': 'Please choose',
    }
    PasswordInput = {
        'type': 'Password',
        'required': 'Please choose',
    }
    error_messages = {
        'required': 'Please type',
    }
    pos_attrs = {
        'type': 'text',
        'class': 'todo-form-input',
        'placeholder':'Your username',
        'required': 'Please type',
    }
    email_attrs = {
        'type': 'text',
        'class': 'todo-form-input',
        'required': 'Please type',
        'placeholder':'Your email',
    }
    first_attrs = {
        'type': 'text',
        'class': 'todo-form-input',
        'required': 'Please type',
        'placeholder':'Your first name',
    }
    last_attrs = {
        'type': 'text',
        'class': 'todo-form-input',
        'required': 'Please type',
        'placeholder':'Your last name',
    }
    username = forms.CharField(label='Username', required=True, max_length=40, widget=forms.TextInput(attrs=pos_attrs))
    email = forms.EmailField(label = 'Email', required=True, max_length = 120, widget=forms.TextInput(attrs=email_attrs))
    password = forms.CharField(label='Password', max_length=32, widget=forms.PasswordInput)
    password1 = forms.CharField(label='Password Confirm', max_length=32, widget=forms.PasswordInput)
    first_name = forms.CharField(label='First Name', required=True, max_length=40, widget=forms.TextInput(attrs=first_attrs))
    last_name = forms.CharField(label='Last Name', required=True, max_length=40, widget=forms.TextInput(attrs=last_attrs))
    birth_date = forms.DateField(label='Birth Date', widget = forms.TextInput(attrs=Date), required=True)

class LoginForm(forms.Form):
  name = forms.CharField(max_length=40, widget=forms.TextInput(attrs={
    'class': 'form-control',
  }))
  password = forms.CharField(max_length=32, widget=forms.PasswordInput(attrs={
    'class': 'form-control',
  }))
