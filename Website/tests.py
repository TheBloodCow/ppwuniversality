from django.test import TestCase, Client
from django.urls import resolve
from django.apps import apps
from .apps import WebsiteConfig
from .views import profile, addPost, deleteAll, signUp, saveAccount, forum, saveproject, bookPage, login, logout
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time


response={}
found={}
class UniversalTest(TestCase):
    def test__url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)
    def test__using_index_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'homePage.html')
    def test__using_profile_func(self):
        found = resolve('/')
        self.assertEqual(found.func, profile)
    def test__using_addPost_func(self):
        found = resolve('/extension/addPost')
        self.assertEqual(found.func, addPost)
    def test__using_forum_func(self):
        found = resolve('/extension/forum')
        self.assertEqual(found.func, forum)
    def test__using_login_func(self):
        found = resolve('/extension/login')
        self.assertEqual(found.func, login)
    def test__using_logout_func(self):
        found = resolve('/extension/logout')
        self.assertEqual(found.func, logout)
    def test__using_signUp_func(self):
        found = resolve('/extension/signUp')
        self.assertEqual(found.func, signUp)
    def test__using_bookPage_func(self):
        found = resolve('/extension/bookPage')
        self.assertEqual(found.func, bookPage)
    def test_apps(self):
        self.assertEqual(WebsiteConfig.name, 'Website')
        self.assertEqual(apps.get_app_config('Website').name, 'Website')
"""
class FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FunctionalTest, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(FunctionalTest, self).tearDown()
    def test_OpenDeBrowser(self):
        self.browser.get("http://127.0.0.1:8000/")
        self.browser.find_element_by_css_selector('.xsa').click()
        time.sleep(2)
        text_box = self.browser.find_element_by_class_name('todo-form-input')
        text_box.send_keys("this is a test")
        self.browser.find_element_by_css_selector('.xsc').click()
        time.sleep(2)
        self.assertIn("this is a test", self.browser.page_source)
        self.browser.quit()
    def test_myTextThere(self):
        self.browser.get("http://127.0.0.1:8000/")
        self.assertNotEqual(len(self.browser.find_elements_by_tag_name("p")), 0)
        print("Done")
        self.browser.quit()
    def test_myNameThere(self):
        self.browser.get("http://127.0.0.1:8000/")
        self.assertNotEqual(len(self.browser.find_elements_by_tag_name("h2")), 0)
        print("Done1")
        self.browser.quit()
    def test_cssNameFont(self):
        self.browser.get("http://127.0.0.1:8000/")
        self.assertEqual(self.browser.find_element_by_tag_name("h2").value_of_css_property('color'), "rgba(0, 0, 0, 1)")
        print("Done2")
        self.browser.quit()
    def test_cssParagraphFont(self):
        self.browser.get("http://127.0.0.1:8000/")
        nameElement = self.browser.find_element_by_class_name("simpleDesc")
        self.assertEqual(nameElement.value_of_css_property('font-size'),"14px")
        print("Done2")
        self.browser.quit()

    if __name__ =='__main__':
        unittest.main(warnings='ignore')
"""
