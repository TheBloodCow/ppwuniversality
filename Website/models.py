from django.db import models
from django.template.defaultfilters import slugify
from django.contrib.auth.models import User
from django.utils import timezone
from django import forms
from django.db.models.signals import post_save
from django.dispatch import receiver

# Create your models here.
class OnePost(models.Model):
    Status = models.CharField(max_length=40);
class Account(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    first_name = models.CharField(max_length = 40)
    last_name = models.CharField(max_length = 40)
    email = models.EmailField(max_length = 120)
    password1 = models.CharField(max_length = 120)
    birth_date = models.DateField()
