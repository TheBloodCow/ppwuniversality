from django.conf import settings
from django.conf.urls.static import static
from django.urls import include, path, re_path
#from django.conf.urls import patterns, url
from django.contrib.auth import urls
from django.conf.urls import url
from django.contrib import admin
from Website.urls import urlpatterns as account_urls
from Website.views import profile

urlpatterns = [
    path('admin/', admin.site.urls),
    path('extension/', include(account_urls)),
    path('', profile, name ='profile'),
    url(r'^auth/', include('social_django.urls', namespace='social'))
]
